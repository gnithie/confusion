import { Injectable } from '@angular/core';

import { Feedback } from '../shared/feedback';

import { RestangularModule, Restangular } from 'ngx-restangular';
import { baseURL } from '../shared/baseurl';
import { Observable } from 'rxjs/Observable';
import { ProcessHttpmsgService } from './process-httpmsg.service';

import 'rxjs/add/operator/map';

@Injectable()
export class FeedbackService {

  constructor(private restangular: Restangular,
    private ProcessHttpmsgService: ProcessHttpmsgService) { }

  submitFeedback(feedback: Feedback): Observable<Feedback>{
    return this.restangular.all('feedback').post(feedback);
  }

}
