import { Component, OnInit, Inject } from '@angular/core';

import { Leader } from '../shared/leader';
import { LeaderService } from '../services/leader.service';
import { flyInOut, expand } from '../animations/app.animation';

import { Params,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display:block;'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class AboutComponent implements OnInit {

  //leader: Leader;
  leaders: Leader[];

  constructor(private leaderservice: LeaderService,
              private route: ActivatedRoute,
              @Inject('BaseURL') private BaseURL) { }

  ngOnInit() {
    //let id = this.route.snapshot.params['id'];
    //this.leader = this.leaderservice.getLeader(id);
    this.leaderservice.getLeaders()
      .subscribe(leaders => this.leaders = leaders);
  }

}
